<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

//
////SharpSpring coupon form code
add_action( 'gform_after_submission_3', 'post_to_third_party_3', 10, 2 );

function post_to_third_party_3( $entry, $form ) {
    $baseURI = 'https://app-3QNH4YWUT8.marketingautomation.services/webforms/receivePostback/MzawMDE3MjE1AgA/';
    $endpoint = '0a0b9530-98cd-4d27-97bf-c2cab3c684c6';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'First' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'email' => rgar( $entry, '3' ),
        'phone' => rgar( $entry, '4' ),
        'terms' => rgar( $entry, '7' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

//SharpSpring contact us form code
add_action( 'gform_after_submission_4', 'post_to_third_party_4', 10, 2 );

function post_to_third_party_4( $entry, $form ) {
    $baseURI = 'https://app-3QNH4YWUT8.marketingautomation.services/webforms/receivePostback/MzawMDE3MjE1AgA/';
    $endpoint = '54447b2e-7c22-493c-997e-9d4f3a50ebd2';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'email' => rgar( $entry, '3' ),
        'phone' => rgar( $entry, '4' ),
        'questions comments' => rgar( $entry, '8' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

// SharpSpring in-home estimate form code
add_action( 'gform_after_submission_14', 'post_to_third_party_14', 10, 2 );

function post_to_third_party_14( $entry, $form ) {
    $baseURI = 'https://app-3QNH4YWUT8.marketingautomation.services/webforms/receivePostback/MzawMDE3MjE1AgA/';
    $endpoint = '3b5f3439-b26c-463f-a571-98bd3f588e62';
    $post_url = $baseURI . $endpoint;

    $body = array(
        'first' => rgar( $entry, '1' ),
        'last' => rgar( $entry, '2' ),
        'email' => rgar( $entry, '3' ),
        'phone' => rgar( $entry, '4' ),
        'time' => rgar( $entry, '10' ),
        'date' => rgar( $entry, '11' ),
        'question comment' => rgar( $entry, '8' ),
        'trackingid__sb' => $_COOKIE['__ss_tk']
    );
    $request = new WP_Http();

    $response = $request->post( $post_url, array( 'body' => $body ) );
}

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
});

// Disable Updraft and Wordfence on localhost
add_action( 'init', function () {
    if ( !is_admin() && ( strpos( get_site_url(), 'localhost' ) > -1 ) ) {
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

        $updraft = WP_PLUGIN_DIR . '/updraftplus/updraftplus.php';
        if ( file_exists( $updraft ) ) deactivate_plugins( [ $updraft ] );

        $wordfence = WP_PLUGIN_DIR . '/wordfence/wordfence.php';
        if ( file_exists( $wordfence ) ) deactivate_plugins( [ $wordfence ] );
    }
    register_shortcodes();
} );

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );



//* Remove Query String from Static Resources
function remove_css_js_ver( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_css_js_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_css_js_ver', 10, 2 );


// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


function register_shortcodes(){
  $dir=dirname(__FILE__)."/shortcodes";
  $files=scandir($dir);
  foreach($files as $k=>$v){
    $file=$dir."/".$v;
    if(strstr($file,".php")){
      $shortcode=substr($v,0,-4);
      add_shortcode($shortcode,function($attr,$content,$tag){
        ob_start();
        include(dirname(__FILE__)."/shortcodes/".$tag.".php");
        $c=ob_get_clean();
        return $c;
      });
    }
  }
}



// Facetwp results
add_filter( 'facetwp_result_count', function( $output, $params ) {
    //$output = $params['lower'] . '-' . $params['upper'] . ' of ' . $params['total'] . ' results';
    $output =  $params['total'] . ' Products';
    return $output;
}, 10, 2 );
// Facetwp results pager
function my_facetwp_pager_html( $output, $params ) {
    $output = '';
    $page = $params['page'];
    $total_pages = $params['total_pages'];
    if ( $page > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page - 1) . '"><span class="pager-arrow"><</span></a>';
    }
    $output .= '<span class="pager-text">page ' . $page . ' of ' . $total_pages . '</span>';
    if ( $page < $total_pages && $total_pages > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page + 1) . '"><span class="pager-arrow">></span></a>';
    }
    return $output;
}

add_filter( 'facetwp_pager_html', 'my_facetwp_pager_html', 10, 2 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Disable gravity forms editor
function remove_gf_notification_visual_editor($settings, $editor_id)
{
    if ($editor_id === 'gform_notification_message') {
    $settings['tinymce'] = false;
}
    return $settings;
}

add_filter('wp_editor_settings', 'remove_gf_notification_visual_editor', 10, 2);


remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds


if(@$_GET['keyword'] != '' && @$_GET['brand'] !="")
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
  	    setcookie('keyword' , $_GET['keyword']);
	    setcookie('brand' , $_GET['brand']);
	    wp_redirect( $url[0] );
	    exit;
 } 
 else if(@$_GET['brand'] !="" && @$_GET['keyword'] == '' )
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
        setcookie('brand' , $_GET['brand']);
        wp_redirect( $url[0] );
        exit;
 }
 else if(@$_GET['brand'] =="" && @$_GET['keyword'] != '' )
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
        setcookie('keyword' , $_GET['keyword']);
        wp_redirect( $url[0] );
        exit;
 }
 
 
// shortcode to show H1 google keyword fields
function new_google_keyword() 
{
      
   if( $_COOKIE['keyword'] ==""  && $_COOKIE['brand'] == "")
   {
        // return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring<h1>';
        return $google_keyword = '<h1 class="googlekeyword">SAVE UP TO $500 ON FLOORING*<h1>';
   }
   else
   {
       $keyword = $_COOKIE['keyword'];
	   $brand = $_COOKIE['brand'];
    //    return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on '.$brand.' '.$keyword.'<h1>';
       return $google_keyword = '<h1 class="googlekeyword">SAVE UP TO $500 ON '.$brand.' '.$keyword.'<h1>';
   }
}
  add_shortcode('google_keyword_code', 'new_google_keyword');
  add_action('wp_head','cookie_gravityform_js');

  function cookie_gravityform_js()
  { // break out of php 
  ?>
  <script>
	  var brand_val ='<?php echo @$_COOKIE['brand'];?>';
	  var keyword_val = '<?php echo @$_COOKIE['keyword'];?>';  

      jQuery(document).ready(function($) {
      jQuery("#input_3_9").val(keyword_val);
      jQuery("#input_3_10").val(brand_val);
    });
  </script>
  <?php  
     setcookie('keyword' , '',-3600); 
     setcookie('brand' , '',-3600);
  
}

// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head() {
    
   ?>
      <style>
          .googlekeyword {
             text-align:center;
             color: #fff;
             text-transform: capitalize;   
			  font-size:2.5em !important;
           }
      </style>  
   <?php    
}
function new_year_number() 
{
	return $new_year = date('Y');
}
add_shortcode('year_code_4', 'new_year_number');

function new_year_number_2() 
{
	return $new_year = date('y');
}
add_shortcode('year_code_2', 'new_year_number_2');

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );


function filter_site_upload_size_limit( $size ) {
    $size = 512 * 1024 * 1024;
    return $size;
}
add_filter( 'upload_size_limit', 'filter_site_upload_size_limit', 20 );


